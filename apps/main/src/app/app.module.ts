import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { environment } from '../environments/environment';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      [
        {
          path: 'home',
          loadChildren: () =>
            import('@nx-redbarba/feature/home').then((m) => m.HomeModule),
        },
        { path: '**', pathMatch: 'full', redirectTo: 'home' },
      ],
      { enableTracing: !environment.production }
    ),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
