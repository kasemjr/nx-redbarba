#!/bin/bash

FEATURE_NAME=$1

FEATURE_PATH="feature"

echo "GENERATING STATE FOR FEATURE \"$FEATURE_NAME\""

nx g ngrx $FEATURE_NAME \
  --project $FEATURE_PATH-$FEATURE_NAME \
  --module ./libs/$FEATURE_PATH/$FEATURE_NAME/src/lib/$FEATURE_NAME.module.ts \
  --facade true \
  --root false \
  --barrels true
