#!/bin/bash

FEATURE_NAME=$1

echo "GENERATING FEATURE \"$FEATURE_NAME\""

nx g lib \
  --simple-module-name \
  --directory feature \
  --routing true \
  --lazy \
  --name $FEATURE_NAME
