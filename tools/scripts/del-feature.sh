#!/bin/bash

FEATURE_NAME=$1

echo "DELETING FEATURE \"$FEATURE_NAME\""

nx g remove feature-$FEATURE_NAME
