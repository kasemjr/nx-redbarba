# 1.0.0 (2021-03-15)


### Bug Fixes

* **ci:** correção da variável de ambiente do ci ([7e5e4e4](https://gitlab.com/kasemjunior/nx-redbarba/commit/7e5e4e4c0ee62e8e480a1338819d20eccac47b21))
* **ci:** correcao do script de ci ([eced2f5](https://gitlab.com/kasemjunior/nx-redbarba/commit/eced2f5b6a3108b4dfbba8617366fc8f5c217302))
* **ci:** removido configuração de teste ([be61bb8](https://gitlab.com/kasemjunior/nx-redbarba/commit/be61bb8dfc8dff620721c8f69386379a4306818d))
* **package.json:** adicionado configuração do repo no package.json ([94789c3](https://gitlab.com/kasemjunior/nx-redbarba/commit/94789c350e5b2ce4af7c1de5a9440b04f27535df))
* **semantic-release:** adicionado dependencias que estavam ausentes no package.json ([9fb0f87](https://gitlab.com/kasemjunior/nx-redbarba/commit/9fb0f8729c52d3ff656a6ffa59bad96a381ccd08))
* **semantic-release:** correcao das configuracoes do semantic-release ([b9a1905](https://gitlab.com/kasemjunior/nx-redbarba/commit/b9a1905d9a7029d4a30c7dde8c579d452ef8707f))
